from PIL import Image
import random

def getTrainingImage():
    r = random.randint(1, 4)
    #r = random.randint(1, 46)
    im = Image.open("./data/Images/Tagged/Training/Black_White/"+str(r)+".jpg")
    im.resize((2, 2)).save("procImg.jpg")
    #im.resize((128,128)).save("procImg.jpg")
    im = Image.open("procImg.jpg").load()
    imageWidth, imageHeight = (2, 2)
    #imageWidth, imageHeight = (128, 128)
    #rgbIm = im.convert('RGB')

    grayImage = []

    # Converting the Image to Grayscale
    for h in range(imageHeight):
        for w in range(imageWidth):
            procPixel = (im[w, h][0]+im[w, h][1]+im[w, h][2])/3
            # print procPixel
            grayImage.append(int(procPixel))

    return (r-1, grayImage)

def getTestImage(path):
    im = Image.open(path)
    #im.resize((2,2)).save("procImg.jpg")
    im.resize((128,128)).save("procImg.jpg")
    im = Image.open("procImg.jpg").load()
    #imageWidth, imageHeight = (2, 2)
    imageWidth, imageHeight = (128, 128)

    grayImage = []

    # Converting the Image to Grayscale
    for h in range(imageHeight):
        for w in range(imageWidth):
            procPixel = (im[w, h][0]+im[w, h][1]+im[w, h][2])/3
            # print procPixel
            grayImage.append(int(procPixel))
    return grayImage



