import sys
import network
import json
import random
import imageParser as ip
import views
import network_interface
import _tkinter
from tkinter import *
from tkinter import ttk
from ttkthemes import themed_tk as tk
from time import gmtime, strftime
from PIL import Image, ImageTk

class App():
    def __init__(self, master, pa, pb, pc, pd):
        self.pad = str(pa)+" "+str(pb)+" "+str(pc)+" "+str(pd)
        self.master = master
        self.mainframe = ttk.Frame(self.master)
        self.mainframe.pack(fill=BOTH, expand=1)
        # self.master.columnconfigure(0, weight=1)
        # self.master.rowconfigure(0, weight=1)

def doCycle(nNet, name, data, datatag):

    nNet.guess(data)
    nNet.backpropagate(datatag)
    with open("data/saves/"+name+".json", 'w') as writefile:
        json.dump(nNet.savenet(), writefile)
    time = strftime('%d-%m-%Y_%H-%M', gmtime())
    procTuple = (time, nNet.error(datatag))
    toterror[cycles] = procTuple


if __name__ == '__main__':
    root = tk.ThemedTk()
    root.title('Foculus')
    root.xsize = 900
    root.ysize = 600
    root.geometry(str(root.xsize)+'x'+str(root.ysize))
    root.set_theme("arc")

    app = App(root, 1, 1, 1, 1)
    cNet = network.Network(4, 2, 8, 2, "Black_White_4x4")
    #cNet = network.Network(16384, 1, 128, 2, "Foculus_128x128")

    imagenumber = 2
    initimagepath = "data/Images/Tagged/Training/"+str(imagenumber)+".jpg"
    initimage = Image.open(initimagepath)
    resizedinitimage = initimage.resize((int(root.ysize/2),int(root.ysize/2)))
    initphotoimage = ImageTk.PhotoImage(resizedinitimage)
    network_handler = network_interface.NetworkHandler(cNet)
    
    views.HomeView(app, initphotoimage, network_handler)


    root.mainloop()
    
    #app = QApplication(sys.argv)
    #mw = MainWindow(cNet)

    # cNet = network.Network(7, 1, 5, 2)
    # testvalues = [1, 2, 3, 4, 9, 20, 10]

    # cNet.guess(testvalues)
    # tags = [1, 1, 1, 0, 0, 0]
    # trainingData = [[1, 1,
    #                  1, 1],
    #
    #                 [1, 1,
    #                  1, 0],
    #
    #                 [1, 1,
    #                  0, 1],
    #
    #                 [0, 0,
    #                  0, 0],
    #
    #                 [0, 0,
    #                  0, 1],
    #
    #                 [0, 0,
    #                  1, 0]]

    # with open("data/saves/Begin.json", "w") as writefile:
    #     json.dump(cNet.savenet(), writefile)

    # with open("data/saves/Version_2.json", "r") as readfile:
    #    netdata = json.load(readfile)

    # cNet.loadnet(netdata)

    # testdata = ip.getTestImage(1)
    # cNet.guess(testdata)
    # testdata = ip.getTestImage(3)
    # cNet.guess(testdata)
    # for i in range(100):
    #     trainingData = ip.getTrainingImage()
    #     trainingTag = tags[trainingData[0]]
    #     trainingImage = trainingData[1]
        # rand = random.randint(0, 5)
        # trainingImage = trainingData[rand]
        # trainingTag = tags[rand]
    #     cycles += 1
    #     doCycle(cNet, 'Alpha', trainingImage, trainingTag)

    # with open("data/saves/Alpha.json", "w") as writefile:
    #     json.dump(cNet.savenet(), writefile)
    # with open("data/saves/toterror.json", "w") as writefile:
    #     json.dump(toterror, writefile)





    # matrix1Values = [[1, 2, 3]]
    # matrix1 = mc.Matrix(matrix1Values)
    # matrix2 = mc.Matrix(matrix1Values)
    #
    # sum =  matrix1.scalar(matrix2)
    # print sum
    # print cNet.inputLayer
    # print cNet.hiddenLayers
    # print cNet.outputLayer
    # print cNet.inputLayer[0].weights
    # sys.exit(app.exec_())



