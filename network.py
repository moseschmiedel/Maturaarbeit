import neuron as n
import vector as vc
import numpy as np
import math
import random
from decimal import getcontext, Decimal


# Set Decimalprecision
getcontext().prec = 2

class Network:
    """Class to create a Network from Neurons"""

    def __init__(self, inputLayerNC, hiddenLayerC, hiddenLayerNC, outputLayerNC, networkName):

        # Class Data
        self.name = networkName

        # Layers
        self.inputLayer = []
        self.hiddenLayers = {}
        self.outputLayer = []

        self.cvalues = []
        self.toterror = []
        self.targets = [0.0, 0.0]
        self.cost = 0
        self.cycle = 0
        self.layers = 2
        self.alpha = 0.1
        self.bias = 1.0

        # Creating the wanted amount of hidden Layers
        for c in range(hiddenLayerC):
            self.hiddenLayer = []
            procDict = {c: self.hiddenLayer}
            self.hiddenLayers.update(procDict)
            self.layers += 1

        # Filling the Layers with the wanted amount of Neurons
        for i in range(inputLayerNC):
            ineuron = n.Neuron(1)
            self.inputLayer.append(ineuron)

        for h in range(hiddenLayerNC):
            hneuron = n.Neuron(hiddenLayerNC)
            hzneuron = n.Neuron(inputLayerNC)
            # print hzNeuron.weights.columns
            for key in list(self.hiddenLayers):
                if key == 0:
                    self.hiddenLayers[key].append(hzneuron)
                else:
                    self.hiddenLayers[key].append(hneuron)

        for o in range(outputLayerNC):
            oneuron = n.Neuron(hiddenLayerNC+1)
            self.outputLayer.append(oneuron)

    def addNewNeuron(self, layerType, weights):
        # Creating a new Neuron
        neurone = n.Neuron(weights)

        # Sorting Neuron into one Layer
        if layerType is 'input':
            self.inputLayer.append(neurone)
        elif layerType is 'hidden':
            self.hiddenLayer.append(neurone)
        elif layerType is 'output':
            self.outputLayer.append(neurone)
        else:
            print('Argument not defined')

    def addExistingNeuron(self, neuron, layerType):
        # Sorting Neuron into one Layer
        if layerType is 'input':
            self.inputLayer.append(neuron)
        elif layerType is 'hidden':
            self.hiddenLayer.append(neuron)
        elif layerType is 'output':
            self.outputLayer.append(neuron)
        else:
            print('Argument not defined')

    def feedForward(self, ninput):
        # Activating Inputlayer
        self.cvalues = {}
        procList = []
        for n in range(len(self.inputLayer)):
            o = ninput[n]
            procList.append(o)
        self.cvalues['inputlayer'] = procList
        cmatrix = vc.Vector(procList)

        # Activating Hiddenlayers
        c = 0   # Counter for the Hidden Layer
        for hl in list(self.hiddenLayers):
            procList = []
            for n in self.hiddenLayers[hl]:
                o = n.activate(cmatrix)
                procList.append(o)
            if c == (len(self.hiddenLayers)-1):
                procList.append(self.bias)
            self.cvalues['hiddenlayer'+str(c)] = procList
            cmatrix = vc.Vector(procList)
            c += 1

        # Activating Outputlayer
        procList = []
        for n in self.outputLayer:
            o = n.activate(cmatrix)
            procList.append(o)
        self.cvalues['outputlayer'] = procList


    def guess(self, netinput):
        self.feedForward(netinput)
        pro = self.cvalues['outputlayer'][0]
        con = self.cvalues['outputlayer'][1]
        evr = pro + con
       # print self.cvalues[0]
       #  print self.cvalues[1]
       # print evr

        ppro = (Decimal(pro)/Decimal(evr))*100
        pcon = (Decimal(con)/Decimal(evr))*100

        if pro > con:
            print('The person is concentrated!')
            print('Percentages: Pro: ' + str(ppro) + ' Con: ' + str(pcon))

        else:
            print('The person is not concentrated!')
            print('Percentages: Pro: ' + str(ppro) + ' Con: ' + str(pcon))

    def error(self, tag):
        if tag == 0.0:
            self.targets[0] = 0.0
            self.targets[1] = 1.0
        elif tag == 1.0:
            self.targets[0] = 1.0
            self.targets[1] = 0.0
        else:
            print('No legal Tag given!')
            pass

        error1 = self.targets[0]-float(self.cvalues['outputlayer'][0])
        error1 = (error1 * error1)/2
        error2 = self.targets[1] - float(self.cvalues['outputlayer'][1])
        error2 = (error2 * error2)/2

        error = error1 + error2

        return error

    def backpropagate(self, tag):
        targets = [1.0, 0.0]
        if tag == 0:                    # computing Tag
            self.targets[0] = 0.0
            self.targets[1] = 1.0
        elif tag == 1:
            self.targets[0] = 1.0
            self.targets[1] = 0.0
        else:
            print('No legal Tag given!')
            pass

        hc = len(self.hiddenLayers)     # amount of hiddenlayers
        hl = self.hiddenLayers[hc - 1]  # last hiddenlayer
        counter = 0
        for oneuron in self.outputLayer:        # calculating Error of the Neurons of the Outputlayer
            procWeights = []
            for hneuron in range(len(hl)+1):
                procWeight = oneuron.weightList[hneuron]
                oneuron.nettoweight[hneuron] = self.cvalues['hiddenlayer'+ str(hc - 1)][hneuron]
                oneuron.outtonet = self.cvalues['outputlayer'][counter] * (1 - self.cvalues['outputlayer'][counter])
                oneuron.toterrortoout = -(self.targets[counter] - self.cvalues['outputlayer'][counter])
                oneuron.toterror = oneuron.nettoweight[hneuron] * oneuron.outtonet * oneuron.toterrortoout
                # self.toterror.append(oneuron.toterror)

                procWeights.append(procWeight - self.alpha * oneuron.toterror * self.cvalues['outputlayer'][counter])
                # print procWeights[hneuron]
            oneuron.getnewweights(procWeights)
            counter += 1

        for hlayer in range(len(self.hiddenLayers)):     # calculating Error of the Neurons of the Hiddenlayers
            currenthlayer = hc-hlayer-1
            hcount = hc - 1
            hneuroncounter = 0
            for hneuron in self.hiddenLayers[currenthlayer]:
                procWeights = []
                if currenthlayer == hcount:
                    oneuroncounter = 0
                    for oneuron in self.outputLayer:
                        hneuron.errortonet[oneuroncounter] = oneuron.toterrortoout * oneuron.outtonet
                        hneuron.nettoout[oneuroncounter] = oneuron.weightList[hneuroncounter]
                        hneuron.errortoout[oneuroncounter] = hneuron.errortonet[oneuroncounter] * hneuron.nettoout[oneuroncounter]
                        oneuroncounter += 1

                    for e in hneuron.errortoout:
                        hneuron.toterrortoout += e

                    hneuron.outtonet = self.cvalues['hiddenlayer'+str(currenthlayer)][hneuroncounter] * (1-self.cvalues['hiddenlayer'+str(currenthlayer)][hneuroncounter])

                    if hcount == 0:
                        for ineuron in range(len(self.inputLayer)):
                            hneuron.nettoweight[ineuron] = self.cvalues['inputlayer'][ineuron]
                            hneuron.toterrortoweight[ineuron] = hneuron.toterrortoout * hneuron.outtonet * \
                                                                 hneuron.nettoweight[ineuron]

                            procWeight = hneuron.weightList[ineuron]
                            procWeights.append(procWeight - self.alpha * hneuron.toterrortoweight[ineuron] *
                                               self.cvalues['hiddenlayer'+str(currenthlayer)][hneuroncounter])
                    else:
                        for hneuron2 in range(len(self.hiddenLayers[currenthlayer-1])):
                            hneuron.nettoweight[hneuron2] = self.cvalues['hiddenlayer'+str(currenthlayer)][hneuron2]
                            hneuron.toterrortoweight[hneuron2] = hneuron.toterrortoout * hneuron.outtonet * hneuron.nettoweight[hneuron2]

                            # procWeight = np.float64
                            procWeight = hneuron.weightList[hneuron2]
                            procWeights.append(procWeight - self.alpha * hneuron.toterrortoweight[hneuron2])
                    hneuron.getnewweights(procWeights)
                hneuroncounter += 1

        for on in self.outputLayer:
            on.updateweights()
        for hl in range(len(self.hiddenLayers)):
            for hn in self.hiddenLayers[hl]:
                hn.updateweights()


    def savenet(self):
        saveweights = {}
        procWeights = []
        # for ineuron in self.inputLayer:
        #     procWeights.append(ineuron.weightList)
        # procDict = {'input': procWeights}
        # saveweights.update(procDict)

        for hlayer in range(len(self.hiddenLayers)):
            procWeights = []
            for hneuron in self.hiddenLayers[hlayer]:
                procWeights.append(hneuron.weightList)
            saveweights['hidden'+str(hlayer)] = procWeights 

        procWeights = []
        for oneuron in self.outputLayer:
            procWeights.append(oneuron.weightList)
        saveweights['output'] = procWeights


        savedict = {'alpha': self.alpha,
                    'neuroncounts': [len(self.inputLayer), len(self.hiddenLayers), len(self.hiddenLayers[0]), len(self.outputLayer)],
                    'bias': self.bias,
                    'cvalues': self.cvalues,
                    'weights': saveweights}

        return savedict

    def loadnet(self, savedata):
        self.alpha = savedata["alpha"]
        self.cvalues = savedata["cvalues"]
        self.bias = savedata["bias"]
        for hlayer in range(len(self.hiddenLayers)):
            counter = 0
            for hneuron in self.hiddenLayers[hlayer]:
                hneuron.getnewweights(savedata["weights"]["hidden"+str(hlayer)][counter])
                hneuron.updateweights()
                counter += 1

        counter = 0
        for oneuron in self.outputLayer:
            oneuron.getnewweights(savedata["weights"]["output"][counter])
            oneuron.updateweights()
            counter += 1
