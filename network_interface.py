import sys
import network
import imageParser as ip
import views
import json
import random
import PIL.Image
import PIL.ImageTk
from time import gmtime, strftime
from collections import deque
import _tkinter
from tkinter import *
from tkinter import ttk
from tkinter import filedialog

class NetworkHandler():
    def __init__(self, network):
        self.network = network
        self.trainpath = ""
        self.testpath = ""
        self.tags = [1,1,0,0]
        #self.tags = [1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1]

    def averageList(self, aqueue):
        tsum = 0.0
        counter = 0.0
        for e in aqueue:
            tsum += e
            counter += 1.0
        return tsum/counter

    def train_network(self):
        lasterrors = deque([1.0,1.0,1.0,1.0,1.0])
        cycles = 0
        toterror = {}

        while self.averageList(lasterrors) > 0.05:
            cycles += 1

            lasterrors.popleft()
            
            data = ip.getTrainingImage() 
            tag = self.tags[data[0]]
            image = data[1]
            self.network.guess(image)
            print(cycles)
            
            time = strftime('%d-%m-%Y_%H-%M', gmtime())
            current_error = self.network.error(tag)
            lasterrors.append(current_error)
            toterror[cycles] = (time, current_error)
            with open("data/saves/"+self.network.name+"_toterror.json", 'w') as writefile:
                json.dump(toterror, writefile)

            self.network.backpropagate(tag)
            with open("data/saves/"+self.network.name+".json", 'w') as writefile:
                json.dump(self.network.savenet(), writefile)

        print("Done Training!")

    def test_network(self, app, network_handler):
        testimagepath = filedialog.askopenfilename(initialdir="data/Images/", title="Select Image to Test", filetypes=(("jpeg files","*.jpg"),("all files", "*.*")))    
        
        testimageraw = ip.getTestImage(testimagepath)

        dispimage = PIL.Image.open(str(testimagepath))
        resizeddispimage = dispimage.resize((int(app.master.ysize/2),int(app.master.ysize/2)))
        dispphotoimage = PIL.ImageTk.PhotoImage(resizeddispimage)
        
        views.TestingView(app, dispphotoimage, network_handler)

        self.network.guess(testimageraw)

    
