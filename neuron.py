import numpy as np
import random
import vector as vc
import math


class Neuron(object):
    def __init__(self, weightCount):
        """
        Class to create Neurons
        :param weightCount: number of neurons in Layer before
        """
        self.weightList = []

        for j in range(weightCount):
            self.weight = random.random()
            sign = random.randint(0, 1)
            if sign == 0:
                sign = -1
            self.weight = self.weight * (sign/np.sqrt(weightCount))

            self.weightList.append(self.weight)

        # for w in range(weightCount):
        #     i = random.randint(0, 10)
        #     weightList.append([i])

        # Class Data
        self.weightcount = weightCount
        self.weights = vc.Vector(self.weightList)
        self.newweights = []
        self.toterrortoout = 0
        self.errorweight = 0
        self.outtonet = 0
        self.toterrortoweight = []
        self.errortoweight = []
        self.errortonet = []
        self.outtonet = []
        self.nettoweight = []
        self.nettoout = []
        self.errortoout = []
        for wc in range(weightCount):
            self.errortoweight.append(0)
            self.errortonet.append(0)
            self.nettoweight.append(0)
            self.nettoout.append(0)
            self.errortoout.append(0)
            self.toterrortoweight.append(0)

        # self.bias = random.randint(1, 10)

    def computesum(self, inputs):
        output = self.weights.scalar(inputs)
        #print(output)
        return output

    def activate(self, iinput):
        i = self.computesum(iinput)
        output = self.sigmoid(i)
        return output

    def sigmoid(self, x):
        sig = 1.0 / (1.0 + np.exp(0.0-x))
        return sig

    def dsigmoid(self, y):
        fy = float(y)
        dsig = self.sigmoid(fy)*(self.sigmoid(-fy))
        return dsig

    def getnewweights(self, newwgts):
        self.newweights = newwgts

    def updateweights(self):
        self.weightList = self.newweights
        self.weights = vc.Vector(self.weightList)


