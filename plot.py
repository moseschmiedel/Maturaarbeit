#!/usr/bin/env python

import plotly.plotly as py
import plotly.graph_objs as go
import plotly
import json
import optparse
import sys

usage = "Usage: %prog [options]"
version = "%prog 0.1"
parser = optparse.OptionParser(usage=usage, version=version)
parser.add_option('-f', '--file', dest='file', help='The Jsonfile from which the data should be read')

(options, args) = parser.parse_args()

with open(str(options.file), 'r') as readfile:
	data = json.load(readfile)

newdata = []
for key in data:
	newdata.append(data[key][1])

with open('datafile.json', 'w') as writefile:
	json.dump(newdata, writefile)


'''xvalues = []
for i in range(len(newdata)):
	xvalues.append(i)

trace = go.Scatter(
x = xvalues,
y = newdata,
mode = 'lines',
name = 'lines'
)

data = [trace]
plotly.offline.plot({
    "data": data,
    "layout": go.Layout(title="hello world")
}, auto_open=True)
'''
