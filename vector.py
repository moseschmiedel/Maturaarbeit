
class Vector:
    def __init__(self, values):
        """
        Class for Vectorcalculation
        :param values: Array with values of the Vector
        """
        self.values = values
        self.vlength = len(values)

    def scalar(self, v2):
        # Scalarproduct of two Vectors
        if self.vlength == v2.vlength:
            scalarsum = 0.0
            for c in range(self.vlength):
                scalarsum += self.values[c] * v2.values[c]
            return scalarsum
        else:
            print('For a scalarproduct you need two vectors of the same size!')
            pass
