import sys
import json
import _tkinter
from tkinter import *
from tkinter import ttk
from tkinter import Wm
from tkinter import filedialog



def HomeView(app, initimage, network_handler):
    try:
        app.displayframe.destroy()
    except AttributeError:
        pass
    """
    Default View: appears when you start the program
    """
    app.displayframe = ttk.Frame(app.mainframe)
    app.displayView = "home"
    app.displayframe.pack(fill=BOTH, expand=1)
    # grid(column=0, row=0, sticky=N+S+W+E)
    ttk.Label(app.displayframe).pack(fill=X,pady=40)
    ttk.Label(app.displayframe, text="Foculus",font=("Liberation Sans", 80), anchor=CENTER, justify=CENTER).pack(fill=X)
    # grid(column=2, row=1, sticky=W+E+N)
    ttk.Label(app.displayframe, text="---\n ein kuenstliches\n neuronales Netz, welches\n ihre Augenoeffnung "
                                     "erkennt.", font=("Liberation Sans", 30), anchor=CENTER, justify=CENTER).pack(fill=X)
    # grid(column=2, row=2, sticky=W+E+N)
    ttk.Label(app.displayframe).pack(fill=BOTH, expand=2)
    # grid(column=1, columnspan=3, row=3, sticky=W+E)
    buttonframe = ttk.Frame(app.displayframe)
    buttonframe.pack(fill=X,pady=5)
    buttonframe.columnconfigure(0,weight=1)
    ttk.Button(buttonframe, text="Training", command= lambda: TrainingView(app, initimage, network_handler)).grid(column=0,row=0,padx=5, sticky=W)
    ttk.Button(buttonframe, text="Testing", command= lambda: TestingView(app, initimage, network_handler)).grid(column=2,row=0,padx=5, sticky=E)


def TrainingView(app, currentimage, network_handler):
    try:
        app.displayframe.destroy()
    except AttributeError:
        pass
    """
    Training View: here the Network can be trained and the Files for Training can be set, some plots displaying data of the training process are displayed
    """
    # Events
    def statusupdate(event):
        widget = str(event.widget)
        if widget == ".!frame.!frame2.!frame2.!label":
            print("Updated Statusbar")
            status.set("Current Image used for Training")
        elif widget == ".!frame.!frame2.!frame3.!button":
            print("Updated Statusbar")
            status.set("Switch to Testingview")
        elif widget == ".!frame.!frame2.!frame3.!button2":
            print("Updated Statusbar")
            status.set("Start Training of the Network")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label":
            print("Updated Statusbar")
            status.set("Title of current Network")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label2":
            print("Updated Statusbar")
            status.set("Relative path to current Network")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label3":
            print("Updated Statusbar")
            status.set("Current total error of the Network")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label4":
            print("Updated Statusbar")
            status.set("Past Trainingcycles")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label5":
            print("Updated Statusbar")
            status.set("Path to current Image Set used for Training")
        

    app.master.title = "Training"
    
    # Setting up a Frame for the content of TrainingView()
    app.displayframe = ttk.Frame(app.mainframe)
    app.displayView = "training"
    app.displayframe.pack(fill=BOTH, expand=1)
    # grid(column=0, row=0, sticky='nwes')

    # Splitting the displayframe into different horizontal frames/regions for the stuff to display in
    titleframe = ttk.Frame(app.displayframe)
    titleframe.pack(fill=X)
    contentframe = ttk.Frame(app.displayframe)
    contentframe.pack(fill=BOTH, expand=1)
    buttonframe = ttk.Frame(app.displayframe)
    buttonframe.pack(fill=X,pady=5)
    status = StringVar()
    statusbar = Label(app.displayframe, bg="lightgray", fg="black", textvariable=status, font=("Liberation Sans", 10), justify=LEFT, anchor=W)
    statusbar.pack(fill=X)

    # Titleframe
    ttk.Label(titleframe, text="Training", justify="left", font=("Liberation\ Sans 40 bold")).grid(column=0, row=0,padx=8,pady=3, sticky=W)


    # Contentframe
    contentframe.columnconfigure(0,weight=0)
    contentframe.rowconfigure(0,weight=1)
    contentframe.columnconfigure(1,weight=2)
    # window_height = int(app.master.geometry().split('x')[1].split('+')[0])
    training_image = ttk.Label(contentframe, image=currentimage, anchor=CENTER)
    training_image.grid(column=0,row=0,padx=10,pady=5,sticky=W+N+S)
    # training_image.bind("<Enter>", )
    training_image.bind("<Enter>", statusupdate)

    net_details = Frame(contentframe, bg="lightgray")
    net_details.grid(column=1,row=0,padx=10,pady=55,sticky=W+E+N+S)
    
    net_name = Label(net_details, bg="lightgray", fg="black", text="Foculus", font=("Liberation\ Sans 14 bold"), justify=LEFT, anchor=W)
    net_name.pack(fill=X,pady=5,padx=5)
    net_name.bind("<Enter>", statusupdate)

    net_path = Label(net_details, bg="lightgray", fg="black", text="Path: data/saves/Alpha.json", font=("Liberation\ Sans 12"), justify=LEFT, anchor=W)
    net_path.pack(fill=X,padx=5)
    net_path.bind("<Enter>", statusupdate)

    error = StringVar()
    error.set("Current Networkerror: 0")
    net_error = Label(net_details, bg="lightgray", fg="black", textvariable=error, font=("Liberation\ Sans 12"), justify=LEFT, anchor=W)
    net_error.pack(fill=X,padx=5,anchor=NW)
    net_error.bind("<Enter>", statusupdate)

    cycles = StringVar()
    cycles.set("Trained Cycles: 0")
    net_cycles = Label(net_details, bg="lightgray", fg="black", textvariable=cycles, font=("Liberation\ Sans 12"), justify=LEFT, anchor=W)
    net_cycles.pack(fill=X, padx=5, anchor=NW)
    net_cycles.bind("<Enter>", statusupdate)

    train_set_path = StringVar()
    train_set_path.set("Training Set: data/Images/Tagged/Training/")
    train_set = Label(net_details, bg="lightgray", fg="black", textvariable=train_set_path, font=("Liberation\ Sans 12"), justify=LEFT, anchor=W)
    train_set.pack(fill=X, padx=5, anchor=NW)
    train_set.bind("<Enter>", statusupdate)



    # Buttonframe
    buttonframe.columnconfigure(0,weight=1)
    buttonframe.rowconfigure(0,weight=1)
    testing_button = ttk.Button(buttonframe, text="Testing", command= lambda: TestingView(app, currentimage, network_handler))
    testing_button.grid(column=4,row=0,padx=5,sticky=E+N+S)
    testing_button.bind("<Enter>", statusupdate)

    train_button = ttk.Button(buttonframe, text="Train Network", command= lambda: network_handler.train_network())
    train_button.grid(column=0,row=0,padx=5,sticky=W+N+S)
    train_button.bind("<Enter>", statusupdate)


def TestingView(app, testimage, network_handler):
    try:
        app.displayframe.destroy()
    except AttributeError:
       pass 
    """
    Testing View: here the Network can be tested through inputting custom Pictures, also some plots may be displayed
    """
    
    # Events
    def statusupdate(event):
        widget = str(event.widget)
        if widget == ".!frame.!frame2.!frame2.!label":
            print("Updated Statusbar")
            status.set("Current Image used for Testing")
        elif widget == ".!frame.!frame2.!frame3.!button":
            print("Updated Statusbar")
            status.set("Switch to Trainingview")
        elif widget == ".!frame.!frame2.!frame3.!button2":
            print("Updated Statusbar")
            status.set("Test the current Network")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label":
            print("Updated Statusbar")
            status.set("Title of current Network")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label2":
            print("Updated Statusbar")
            status.set("Relative path to current Network")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label3":
            print("Updated Statusbar")
            status.set("Current Recognitionrate of the Network")
        elif widget == ".!frame.!frame2.!frame2.!frame.!label4":
            print("Updated Statusbar")
            status.set("Relative path to current Image used for Testing")
        

    app.master.title = "Testing"
    
    # Setting up a Frame for the content of TrainingView()
    app.displayframe = ttk.Frame(app.mainframe)
    app.displayView = "testing"
    app.displayframe.pack(fill=BOTH, expand=1)
    # grid(column=0, row=0, sticky='nwes')

    # Splitting the displayframe into different horizontal frames/regions for the stuff to display in
    titleframe = ttk.Frame(app.displayframe)
    titleframe.pack(fill=X)
    contentframe = ttk.Frame(app.displayframe)
    contentframe.pack(fill=BOTH, expand=1)
    buttonframe = ttk.Frame(app.displayframe)
    buttonframe.pack(fill=X,pady=5)
    status = StringVar()
    statusbar = Label(app.displayframe, bg="lightgray", fg="black", textvariable=status, font=("Liberation Sans", 10), justify=LEFT, anchor=W)
    statusbar.pack(fill=X)

    # Titleframe
    ttk.Label(titleframe, text="Testing", justify="left", font=("Liberation\ Sans 40 bold")).grid(column=0, row=0,padx=8,pady=3, sticky=W)


    # Contentframe
    contentframe.columnconfigure(0,weight=0)
    contentframe.rowconfigure(0,weight=1)
    contentframe.columnconfigure(1,weight=2)
    # window_height = int(app.master.geometry().split('x')[1].split('+')[0])
    testing_image = ttk.Label(contentframe, image=testimage, anchor=CENTER)
    testing_image.grid(column=0,row=0,padx=10,pady=5,sticky=W+N+S)
    # training_image.bind("<Enter>", )
    testing_image.bind("<Enter>", statusupdate)

    net_details = Frame(contentframe, bg="lightgray")
    net_details.grid(column=1,row=0,padx=10,pady=55,sticky=W+E+N+S)
    
    net_name = Label(net_details, bg="lightgray", fg="black", text="Foculus", font=("Liberation\ Sans 14 bold"), justify=LEFT, anchor=W)
    net_name.pack(fill=X,pady=5,padx=5)
    net_name.bind("<Enter>", statusupdate)

    net_path = Label(net_details, bg="lightgray", fg="black", text="Path: data/saves/Alpha.json", font=("Liberation\ Sans 12"), justify=LEFT, anchor=W)
    net_path.pack(fill=X,padx=5)
    net_path.bind("<Enter>", statusupdate)

    image_recognition = StringVar()
    image_recognition.set("Recognised with 0%")
    net_image_recognition = Label(net_details, bg="lightgray", fg="black", textvariable=image_recognition, font=("Liberation\ Sans 12"), justify=LEFT, anchor=W)
    net_image_recognition.pack(fill=X,padx=5,anchor=NW)
    net_image_recognition.bind("<Enter>", statusupdate)

    # cycles = StringVar()
    # cycles.set("Trained Cycles: 0")
    # net_cycles = Label(net_details, bg="lightgray", fg="black", textvariable=cycles, font=("Liberation\ Sans 12"), justify=LEFT, anchor=W)
    # net_cycles.pack(fill=X, padx=5, anchor=NW)
    # net_cycles.bind("<Enter>", statusupdate)

    image_path = StringVar()
    image_path.set("Image Path: data/Images/Tagged/Testing/"+str(testimage)+".jpg")
    image_path_label = Label(net_details, bg="lightgray", fg="black", textvariable=image_path, font=("Liberation\ Sans 12"), justify=LEFT, anchor=W)
    image_path_label.pack(fill=X, padx=5, anchor=NW)
    image_path_label.bind("<Enter>", statusupdate)



    # Buttonframe
    buttonframe.columnconfigure(0,weight=1)
    buttonframe.rowconfigure(0,weight=1)
    training_button = ttk.Button(buttonframe, text="Training", command= lambda: TrainingView(app, testimage, network_handler))
    training_button.grid(column=5,row=0,padx=5,sticky=E+N+S)
    training_button.bind("<Enter>", statusupdate)

    test_button = ttk.Button(buttonframe, text="Test Network", command= lambda: network_handler.test_network(app, network_handler))
    test_button.grid(column=0,row=0,padx=5,sticky=W+N+S)
    test_button.bind("<Enter>", statusupdate)

    choose_network_button = ttk.Button(buttonframe, text="Choose Network", command= lambda: choose_network(app, testimage, network_handler))
    choose_network_button.grid(column=3,row=0,padx=5,sticky=W+E+S)
    choose_network_button.bind("<Enter>", statusupdate)

def choose_network(app, testimage, network_handler):
    networkpath = filedialog.askopenfilename(initialdir="data/saves/",title="Choose Networkfile",filetypes=(("json files","*.json"),("all files","*.*")))

    with open(networkpath, 'r') as readfile:
        network_data = json.load(readfile)

    network_handler.network.loadnet(network_data)
    TestingView(app, testimage, network_handler)   
